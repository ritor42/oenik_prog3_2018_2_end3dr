﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Repository
{
    using System;
    using System.Collections.Generic;
    using CarShop.Data;

    /// <summary>
    /// Required interface for all repositories to unify accessible methods
    /// </summary>
    public interface IRepository
    {
        /// <summary>
        /// Creates given entity in the database
        /// </summary>
        /// <param name="entity">Entity</param>
        void Create(IEntity entity);

        /// <summary>
        /// Updates given entity in the database
        /// </summary>
        /// <param name="entity">Entity</param>
        void Update(IEntity entity);

        /// <summary>
        /// Deletes given entity in the database
        /// </summary>
        /// <param name="entity">Entity</param>
        void Delete(IEntity entity);

        /// <summary>
        /// Returns all entity from the given type of dBSet
        /// </summary>
        /// <param name="entityType">Type of dBSet</param>
        /// <returns>Collection of entities</returns>
        IEnumerable<IEntity> Read(Type entityType);
    }
}
