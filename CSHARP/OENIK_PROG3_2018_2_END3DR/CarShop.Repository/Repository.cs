﻿// <copyright file="Repository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using CarShop.Data;

    /// <summary>
    /// Repository which implements IRepository to be able to execute CRUD operations.
    /// </summary>
    public class Repository : IRepository
    {
        private static DBEntities dB;

        /// <summary>
        /// Initializes a new instance of the <see cref="Repository"/> class.
        /// </summary>
        public Repository()
        {
            Repository.dB = Repository.dB ?? new DBEntities();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Repository"/> class.
        /// </summary>
        /// <param name="dB">DBContext for unit testing</param>
        public Repository(DBEntities dB)
        {
            Repository.dB = dB;
        }

        /// <summary>
        /// Creates given entity in the database
        /// </summary>
        /// <param name="entity">Entity</param>
        public void Create(IEntity entity)
        {
            if (entity != null && GetDBSet(entity) is DbSet dbSet)
            {
                dbSet.Add(entity);
                Execute(entity);
            }
        }

        /// <summary>
        /// Updates given entity in the database
        /// </summary>
        /// <param name="entity">Entity</param>
        public void Update(IEntity entity)
        {
            if (entity != null)
            {
                Execute(entity);
            }
        }

        /// <summary>
        /// Deletes given entity in the database
        /// </summary>
        /// <param name="entity">Entity</param>
        public void Delete(IEntity entity)
        {
            if (entity != null && GetDBSet(entity) is DbSet dbSet)
            {
                dbSet.Remove(entity);
                Execute(entity);
            }
        }

        /// <summary>
        /// Returns all entity from the given type of dBSet
        /// </summary>
        /// <param name="entityType">Type of dBSet</param>
        /// <returns>Collection of entities</returns>
        public IEnumerable<IEntity> Read(Type entityType)
        {
            if (entityType != null && typeof(IEntity).IsAssignableFrom(entityType))
            {
                List<IEntity> result = new List<IEntity>();

                if (GetDBSet(entityType) is DbSet dbSet)
                {
                    foreach (IEntity entity in dbSet)
                    {
                        result.Add(entity);
                    }
                }

                return result;
            }
            else
            {
                return null;
            }
        }

        private static Type GetEntityType(IEntity entity) => entity.GetType();

        private static DbSet GetDBSet(IEntity entity) => dB.Set(GetEntityType(entity));

        private static DbSet GetDBSet(Type entityType) => dB.Set(entityType);

        private static void Execute(IEntity entity)
        {
            try
            {
                dB.SaveChanges();
            }
            catch (InvalidOperationException ex)
            {
                dB = new DBEntities();

                throw ex;
            }
            catch (Exception ex)
            {
                var entry = dB.Entry(entity);

                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.State = EntityState.Detached;
                        break;
                    case EntityState.Modified:
                        entry.CurrentValues.SetValues(entry.OriginalValues);
                        entry.State = EntityState.Unchanged;
                        break;
                    case EntityState.Deleted:
                        entry.State = EntityState.Unchanged;
                        break;
                }

                throw ex;
            }
        }
    }
}
