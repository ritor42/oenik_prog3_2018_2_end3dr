﻿// <copyright file="ILogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using System.Xml.Linq;
    using CarShop.Data;

    /// <summary>
    /// Required interface for all logics to unify accessible methods
    /// </summary>
    public interface ILogic
    {
        /// <summary>
        /// Sends create request to repository
        /// </summary>
        /// <param name="entity">Entity</param>
        void Create(IEntity entity);

        /// <summary>
        /// Sends delete request to repository
        /// </summary>
        /// <param name="entity">Entity</param>
        void Delete(IEntity entity);

        /// <summary>
        /// Sends update request to repository
        /// </summary>
        /// <param name="entity">Entity</param>
        void Update(IEntity entity);

        /// <summary>
        /// Sends read request to repository
        /// </summary>
        /// <param name="entityType">Type of requested dBSet</param>
        /// <returns>Collection of entities</returns>
        IEnumerable<IEntity> Read(Type entityType);

        /// <summary>
        /// Joins requested types of dBSets with natural join extended with a where condition
        /// </summary>
        /// <param name="mainEntity">First dBSet type</param>
        /// <param name="subEntity">Second dBSet type which will be connected to the first</param>
        /// <param name="condition">Where condition to filter entities</param>
        /// <returns>Collection of collection of entities</returns>
        IEnumerable<object> Join(Type mainEntity, Type subEntity, Predicate<IEntity> condition);

        /// <summary>
        /// Groups requested type of dBSet by the given property extended with a where condition
        /// </summary>
        /// <param name="entityType">dBSet type</param>
        /// <param name="property">Entity column propertyinfo</param>
        /// <param name="condition">Where condition to filter entities</param>
        /// <returns>Collection of objects (IGrouping, Entity)</returns>
        IEnumerable<object> GroupBy(Type entityType, PropertyInfo property, Predicate<IEntity> condition);

        /// <summary>
        /// Joins requested types of dBSets with natural join extended with a where condition and group them after that
        /// </summary>
        /// <param name="mainEntity">First dBSet type</param>
        /// <param name="subEntity">Second dBSet type which will be connected to the first</param>
        /// <param name="condition">Where condition to filter entities</param>
        /// <returns>Collection of group - count</returns>
        IEnumerable<object> GroupByJoin(Type mainEntity, Type subEntity, Predicate<IEntity> condition);

        /// <summary>
        /// Sends read request to repository and filters by given where condition
        /// </summary>
        /// <param name="entityType">dBSet type</param>
        /// <param name="condition">Where condition to filter entities</param>
        /// <returns>Collection of entities</returns>
        IEnumerable<object> Where(Type entityType, Predicate<IEntity> condition);

        /// <summary>
        /// Webinteraction with Java server
        /// </summary>
        /// <param name="name">Customer name</param>
        /// <param name="modelName">Car model name</param>
        /// <param name="price">Car price from the customer</param>
        /// <returns>Response XDocument from Java server</returns>
        XDocument WebInteraction(string name, string modelName, int price);
    }
}
