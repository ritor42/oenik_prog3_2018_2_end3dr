﻿// <copyright file="Logic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Reflection;
    using System.Xml.Linq;
    using CarShop.Data;
    using CarShop.Repository;

    /// <summary>
    /// Logic which implements ILogic to connect Console application with repository.
    /// </summary>
    public class Logic : ILogic
    {
        private const string Url = "http://localhost:8080/CarShop.JavaWeb/CarShopRequest?";
        private readonly IRepository repository;

        /// <summary>
        /// Initializes a new instance of the <see cref="Logic"/> class.
        /// </summary>
        public Logic()
        {
            this.repository = this.repository ?? new Repository();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Logic"/> class.
        /// </summary>
        /// <param name="repository">Repository for unit testing</param>
        public Logic(IRepository repository)
        {
            this.repository = repository ?? throw new ArgumentNullException(nameof(repository));
        }

        /// <summary>
        /// Sends create request to repository
        /// </summary>
        /// <param name="entity">Entity</param>
        public void Create(IEntity entity)
        {
            if (entity != null)
            {
                this.repository.Create(entity);
            }
        }

        /// <summary>
        /// Sends delete request to repository
        /// </summary>
        /// <param name="entity">Entity</param>
        public void Delete(IEntity entity)
        {
            if (entity != null)
            {
                this.repository.Delete(entity);
            }
        }

        /// <summary>
        /// Sends update request to repository
        /// </summary>
        /// <param name="entity">Entity</param>
        public void Update(IEntity entity)
        {
            if (entity != null)
            {
                this.repository.Update(entity);
            }
        }

        /// <summary>
        /// Sends read request to repository
        /// </summary>
        /// <param name="entityType">Type of requested dBSet</param>
        /// <returns>Collection of entities</returns>
        public IEnumerable<IEntity> Read(Type entityType)
        {
            if (entityType != null)
            {
                return this.repository.Read(entityType);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Joins requested types of dBSets with natural join extended with a where condition
        /// </summary>
        /// <param name="mainEntity">First dBSet type</param>
        /// <param name="subEntity">Second dBSet type which will be connected to the first</param>
        /// <param name="condition">Where condition to filter entities</param>
        /// <returns>Collection of collection of entities</returns>
        public IEnumerable<object> Join(Type mainEntity, Type subEntity, Predicate<IEntity> condition)
        {
            if (mainEntity != null && subEntity != null && typeof(IEntity).IsAssignableFrom(mainEntity) && typeof(IEntity).IsAssignableFrom(subEntity))
            {
                condition = condition ?? ((entity) => true);

                PropertyInfo[] properties = mainEntity.GetProperties();
                PropertyInfo[] subProperties = subEntity.GetProperties();

                IEnumerable<IEntity> mainEntities = this.repository.Read(mainEntity);
                IEnumerable<IEntity> subEntities = this.repository.Read(subEntity);

                if (mainEntities != null && subEntities != null)
                {
                    Type oneToManyType = typeof(ICollection<>).MakeGenericType(subEntity);

                    // One-To-One
                    if (properties.Where(x => x.PropertyType == subEntity).FirstOrDefault() is PropertyInfo subProperty)
                    {
                        var join = from m in mainEntities
                                   where condition(m)
                                   join s in subEntities on subProperty.GetValue(m) equals s
                                   select new { m, s };
                        return join;
                    }

                    // One-To-Many
                    else if (properties.Where(x => x.PropertyType == oneToManyType).FirstOrDefault() is PropertyInfo subsProperty)
                    {
                        var join = from m in mainEntities
                                   where condition(m)
                                   from s in subEntities
                                   where (subsProperty.GetValue(m) as IEnumerable<IEntity>).Contains(s)
                                   select new { m, s };

                        return join;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Groups requested type of dBSet by the given property extended with a where condition
        /// </summary>
        /// <param name="entityType">dBSet type</param>
        /// <param name="property">Entity column propertyinfo</param>
        /// <param name="condition">Where condition to filter entities</param>
        /// <returns>Collection of objects (IGrouping, Entity)</returns>
        public IEnumerable<object> GroupBy(Type entityType, PropertyInfo property, Predicate<IEntity> condition)
        {
            if (entityType != null && property != null && typeof(IEntity).IsAssignableFrom(entityType) && entityType.GetProperties().Contains(property))
            {
                condition = condition ?? ((entity) => true);

                IEnumerable<IEntity> entities = this.repository.Read(entityType);
                if (entities != null)
                {
                    var group = from e in entities
                                where condition(e)
                                group e by property.GetValue(e) into g
                                select new { g, Example = g.Select(x => x).FirstOrDefault() };
                    return group;
                }
            }

            return null;
        }

        /// <summary>
        /// Joins requested types of dBSets with natural join extended with a where condition and groups them after that.
        /// </summary>
        /// <param name="mainEntity">First dBSet type</param>
        /// <param name="subEntity">Second dBSet type which will be connected to the first</param>
        /// <param name="condition">Where condition to filter entities</param>
        /// <returns>Collection of objects (Object, Int)</returns>
        public IEnumerable<object> GroupByJoin(Type mainEntity, Type subEntity, Predicate<IEntity> condition)
        {
            var join = this.Join(mainEntity, subEntity, condition);
            if (join != null && join.Count() > 0)
            {
                PropertyInfo propertyInfo = join.First().GetType().GetProperty("m");
                if (propertyInfo != null)
                {
                    return join.GroupBy(x => propertyInfo.GetValue(x)).Select(x => new { x.Key, Stat = new { Count = x.Count() } });
                }
            }

            return null;
        }

        /// <summary>
        /// Sends read request to repository and filters by given where condition
        /// </summary>
        /// <param name="entityType">dBSet type</param>
        /// <param name="condition">Where condition to filter entities</param>
        /// <returns>Collection of entities</returns>
        public IEnumerable<object> Where(Type entityType, Predicate<IEntity> condition)
        {
            if (entityType != null && typeof(IEntity).IsAssignableFrom(entityType))
            {
                condition = condition ?? ((entity) => true);

                IEnumerable<IEntity> entities = this.repository.Read(entityType);
                if (entities != null)
                {
                    var where = from e in entities
                                where condition(e)
                                select new { e };

                    return where;
                }
            }

            return null;
        }

        /// <summary>
        /// Webinteraction with Java server
        /// </summary>
        /// <param name="name">Customer name</param>
        /// <param name="modelName">Car model name</param>
        /// <param name="price">Car price from the customer</param>
        /// <returns>Response XDocument from Java server</returns>
        public XDocument WebInteraction(string name, string modelName, int price)
        {
            if (!string.IsNullOrEmpty(name) && !string.IsNullOrEmpty(modelName))
            {
                string parameters = string.Format("Name={0}&ModelName={1}&Price={2}", name, modelName, price);

                try
                {
                    return XDocument.Load(Url + parameters);
                }
                catch (WebException)
                {
                    return new XDocument();
                }
            }
            else
            {
                return null;
            }
        }
    }
}
