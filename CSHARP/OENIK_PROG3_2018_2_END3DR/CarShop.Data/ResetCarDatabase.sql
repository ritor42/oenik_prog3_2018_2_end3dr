﻿drop table modelextra;
drop table extra;
drop table model;
drop table brand;

CREATE TABLE Brand (
  BrandID int NOT NULL PRIMARY KEY IDENTITY,
  Name varchar(100) NOT NULL,
  CEO varchar(100) DEFAULT NULL,
  Headquarter varchar(100) NOT NULL,
  YearOfFoundation int NOT NULL,
  YearlyIncome numeric(5,2) DEFAULT NULL,
  URL varchar(200) DEFAULT NULL,
);
INSERT INTO Brand VALUES ('Aston Martin','Andy Palmer','Gaydon',1913,1.14,'https://www.astonmartin.com'),('Bayerische Motoren Werke','Harald Krüger','München, Germany',1916,111.23,'https://www.bmw.com'),('Ferrari','Carlos Ghosn','Maranello, Italy',1929,3.92,'https://www.ferrari.com'),('Honda','Takahiro Hachigo','Minato, Tokyo, Japan',1948,138.65,'https://www.honda.com'),('Ford','James Hackett','Dearborn, Michigan, USA',1903,156.78,'https://www.ford.com'),('Toyota','Akio Toyoda','Toyota, Aichi, Japan',1937,265.17,'https://www.toyota.com'),('Nissan','Hiroto Saikawa','Nishi-ku, Yokohama, Japan',1933,107.87,'https://www.nissan-global.com'),('Volkswagen','Matthias Müller','Wolfsburg, Germany',1937,260.03,'https://www.volkswagen.com');

CREATE TABLE Extra (
  ExtraID int NOT NULL PRIMARY KEY IDENTITY,
  CategoryName varchar(100) NOT NULL,
  Name varchar(100) NOT NULL,
  Compatibility varchar(100) NOT NULL,
  BuiltIn varchar(100) NOT NULL,
  Terior varchar(100) NOT NULL,
);
INSERT INTO Extra VALUES ('Security','Immobiliser','Universal','Built in','Interior'),('Comfort','GPS','Universal','Either','Interior'),('Security','Baby car seat','Universal','Portable','Interior'),('Other','Car cover','Model','Portable','Exterior'),('Other','Bluetooth','Model','Built in','Interior'),('Comfort','Air condition','Model','Built in','Interior'),('Comfort','Radio','Model','Built in','Interior'),('Comfort','Tempomat','Model','Built in','Interior'),('Comfort','Sound system','Universal','Built in','Interior'),('Tuning','Xeon Lighting','Model','Built in','Exterior'),('Other','Camera','Universal','Either','Interior'),('Security','Central lock','Model','Built in','Interior');

CREATE TABLE Model (
  ModelID int NOT NULL PRIMARY KEY IDENTITY,
  BrandID int NOT NULL,
  Name varchar(100) NOT NULL,
  ReleaseYear int DEFAULT NULL,
  EngineConfiguration varchar(100) DEFAULT NULL,
  EngineType varchar(100) DEFAULT NULL,
  TankCapacity int DEFAULT NULL,
  TopSpeed int DEFAULT NULL,
  HorsePower int DEFAULT NULL,
  Weight int DEFAULT NULL,
  URL varchar(100) DEFAULT NULL,
  CONSTRAINT BrandFK FOREIGN KEY (BrandID) REFERENCES Brand(BrandID) ON DELETE CASCADE,
);
INSERT INTO Model VALUES (1,'DB9',2009,'V-Engine','Internal Combustion Engine',80,306,477,1760,'https://www.car.info/en-se/aston-martin/db9/1st-generation-1st-facelift-59-v12-m6-6678603/specs'),(1,'DB11',2018,'V-Engine','Internal Combustion Engine',78,300,510,1870,'https://www.car.info/en-se/aston-martin/db11/db11-volante-40-v8-16580177/specs'),(1,'Vantage',2005,'V-Engine','Internal Combustion Engine',80,280,385,1570,'https://www.car.info/en-se/aston-martin/vantage/3rd-generation-43-v8-m6-6646276/specs'),(2,'X1',2016,'Inline','Internal Combustion Engine',35,195,231,NULL,'https://www.car.info/en-se/bmw/x1/f48-15-71-kwh-xdrive-ma6-16181678/specs'),(2,'X4',2014,'Inline','Internal Combustion Engine',67,247,313,1935,'https://www.car.info/en-se/bmw/x4/x4-xdrive35d-6778805/specs'),(2,'X6',2013,'Inline','Internal Combustion Engine',85,250,381,2225,'https://www.car.info/en-se/bmw/x6/x6-m50d-6778756/specs'),(3,'LaFerrari',2015,'V-Engine','Internal Combustion Engine',NULL,360,1050,1265,'https://www.car.info/en-se/ferrari/laferrari/1st-generation-63-v12-dct7-6679211/specs'),(4,'Accord',2003,'Inline','Internal Combustion Engine',65,237,220,1390,'https://www.car.info/en-se/honda/accord/7th-generation-20-vtec-m6-6667940/specs'),(4,'Clarity',2017,'Electric','Electric',NULL,NULL,163,NULL,'https://www.car.info/en-se/honda/clarity/clarity-255-kwh-11155073/specs'),(5,'Focus',NULL,'Inline','Internal Combustion Engine',52,210,150,1294,'https://www.car.info/en-se/ford/focus/mk4-15-ecoboost-m6-17150646/specs'),(5,'Fusion',2017,'V-Engine','Internal Combustion Engine',68,NULL,329,NULL,'https://www.car.info/en-se/ford/fusion-na/fusion-27-v6-ecoboost-awd-11424077/specs'),(8,'e-Golf',2014,'Electric','Electric',NULL,140,115,1585,'https://www.car.info/en-se/volkswagen/golf/a7-typ-5g-242-kwh-s-6652704/specs'),(8,'Arteon',2017,'Inline','Internal Combustion Engine',66,250,280,1716,'https://www.car.info/en-se/volkswagen/arteon/arteon-20-tsi-4motion-11016597/specs'),(8,'Polo',2017,'Inline','Internal Combustion Engine',40,200,116,1155,'https://www.car.info/en-se/volkswagen/polo/polo-5-door-10-tsi-11455942/specs');

CREATE TABLE ModelExtra (
  ModelID int NOT NULL,
  ExtraID int NOT NULL,
  CONSTRAINT Once UNIQUE (ModelID,ExtraID),
  CONSTRAINT ExtraFK FOREIGN KEY (ExtraID) REFERENCES Extra(ExtraID) ON DELETE CASCADE,
  CONSTRAINT ModelFK FOREIGN KEY (ModelID) REFERENCES Model(ModelID) ON DELETE CASCADE,
);

INSERT INTO ModelExtra VALUES (1,1),(1,2),(1,8),(1,10),(1,12),(2,1),(2,3),(2,11),(2,12),(3,1),(3,5),(3,6),(3,12),(4,1),(4,2),(4,3),(4,4),(4,5),(4,6),(4,7),(4,9),(4,11),(4,12),(5,1),(5,2),(5,5),(5,6),(5,10),(5,11),(5,12),(6,2),(6,7),(6,8),(6,12),(7,1),(7,2),(7,4),(7,5),(7,6),(7,7),(7,8),(7,9),(7,10),(7,11),(7,12),(8,1),(9,4),(9,5),(9,7),(9,11),(9,12),(10,3),(10,4),(10,9),(11,2),(11,3),(11,6),(11,7),(11,12),(12,1),(12,2),(12,5),(12,6),(12,7),(12,12),(13,1),(13,4),(13,8),(13,10),(13,11),(13,12),(14,1),(14,2),(14,3),(14,4),(14,5),(14,6),(14,7),(14,9),(14,11),(14,12);