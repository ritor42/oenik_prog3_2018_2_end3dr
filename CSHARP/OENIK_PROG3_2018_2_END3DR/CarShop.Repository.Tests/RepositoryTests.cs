﻿// <copyright file="RepositoryTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Repository.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Threading.Tasks;
    using CarShop.Data;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Unit testing for repository
    /// </summary>
    [TestFixture]
    public class RepositoryTests
    {
        private static readonly DBEntities DB = TestDBContextMoq.MockDbContext();
        private IRepository repo;

        /// <summary>
        /// Setup for unit testing
        /// </summary>
        [SetUp]
        public void Setup()
        {
            this.repo = new Repository(DB);
        }

        /// <summary>
        /// Unit test for Repository create method -> null check
        /// </summary>
        [Test]
        public void Repository_Create_Null()
        {
            IEntity entity = null;

            void act(IEntity e) => this.repo.Create(e);

            Assert.That(() => act(entity), Throws.Nothing);
        }

        /// <summary>
        /// Unit test for Repository create method -> fake objects check
        /// </summary>
        /// <param name="entityType">Entity type</param>
        [Test]
        [Sequential]
        public void Repository_Create_Fake([Values(typeof(Brand), typeof(Model), typeof(Extra))] Type entityType)
        {
            IEntity entity = Activator.CreateInstance(entityType) as IEntity;

            void act(IEntity e) => this.repo.Create(e);

            Assert.That(() => act(entity), Throws.Nothing);
        }

        /// <summary>
        /// Unit test for Repository update method -> null check
        /// </summary>
        [Test]
        public void Repository_Update_Null()
        {
            IEntity entity = null;

            void act(IEntity e) => this.repo.Update(e);

            Assert.That(() => act(entity), Throws.Nothing);
        }

        /// <summary>
        /// Unit test for Repository update method -> fake objects check
        /// </summary>
        /// <param name="entityType">Entity Type</param>
        [Test]
        [Sequential]
        public void Repository_Update_Fake([Values(typeof(Brand), typeof(Model), typeof(Extra))] Type entityType)
        {
            IEntity entity = Activator.CreateInstance(entityType) as IEntity;

            void act(IEntity e) => this.repo.Update(e);

            Assert.That(() => act(entity), Throws.Nothing);
        }

        /// <summary>
        /// Unit test for Repository delete method -> null check
        /// </summary>
        [Test]
        public void Repository_Delete_Null()
        {
            IEntity entity = null;

            void act(IEntity e) => this.repo.Delete(e);

            Assert.That(() => act(entity), Throws.Nothing);
        }

        /// <summary>
        /// Unit test for Repository delete method -> fake objects check
        /// </summary>
        /// <param name="entityType">Entity type</param>
        [Test]
        [Sequential]
        public void Repository_Delete_Fake([Values(typeof(Brand), typeof(Model), typeof(Extra))] Type entityType)
        {
            IEntity entity = Activator.CreateInstance(entityType) as IEntity;

            void act(IEntity e) => this.repo.Delete(e);

            Assert.That(() => act(entity), Throws.Nothing);
        }

        /// <summary>
        /// Unit test for Repository read method -> null check
        /// </summary>
        [Test]
        public void Repository_Read_Null()
        {
            Type entityType = null;

            IEnumerable<IEntity> act(Type t) => this.repo.Read(t);

            Assert.That(() => act(entityType), Throws.Nothing);
            Assert.That(() => act(entityType), Is.EqualTo(null));
        }

        /// <summary>
        /// Unit test for Repository read method -> fake objects check
        /// </summary>
        /// <param name="entityType">Entity Type</param>
        [Test]
        [Sequential]
        public void Repository_Read_Fake([Values(typeof(Brand), typeof(Model), typeof(Extra))] Type entityType)
        {
            IEnumerable<IEntity> act(Type t) => this.repo.Read(t);

            Assert.That(() => act(entityType), Throws.Nothing);
            Assert.That(() => act(entityType), Is.Not.EqualTo(null));
            Assert.That(() => act(entityType).Count(), Is.EqualTo(0));
        }
    }
}
