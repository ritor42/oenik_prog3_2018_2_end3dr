﻿// <copyright file="TestDBContextMoq.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Repository.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CarShop.Data;
    using Coderful.EntityFramework.Testing.Mock;
    using Moq;

    /// <summary>
    /// DBContextMoq to Moq DBContext
    /// </summary>
    internal static class TestDBContextMoq
    {
        /// <summary>
        /// Create DBContext to be able to test repository
        /// </summary>
        /// <returns>DBContext</returns>
        public static DBEntities MockDbContext()
        {
            List<Brand> Brand = new List<Brand>();
            List<Model> Model = new List<Model>();
            List<Extra> Extra = new List<Extra>();

            var mockContext = new Mock<DBEntities>();

            // Create the DbSet objects.
            var dbSets = new object[]
            {
                        MoqUtilities.MockDbSet(Brand, (objects, brand) => brand.BrandID == (int)objects[0]),
                        MoqUtilities.MockDbSet(Model, (objects, model) => model.ModelID == (int)objects[0]),
                        MoqUtilities.MockDbSet(Extra, (objects, extra) => extra.ExtraID == (int)objects[0])
            };

            return new MockedDbContext<DBEntities>(mockContext, dbSets).DbContext.Object;
        }
    }
}
