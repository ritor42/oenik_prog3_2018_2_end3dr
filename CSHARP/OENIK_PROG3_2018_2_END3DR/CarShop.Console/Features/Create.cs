﻿// <copyright file="Create.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Console.Features
{
    using System;
    using System.Data.Entity.Validation;
    using System.Reflection;
    using System.Text;
    using CarShop.Data;

    /// <summary>
    /// Feature which is able to create entity for any entity type.
    /// </summary>
    [Feature("Create")]
    internal class Create : IFeature
    {
        /// <summary>
        /// Displays information about the feature
        /// </summary>
        public void Info()
        {
            Display.Pause("This feature is capable of creating entities." + Environment.NewLine
                        + "First you should select a table where the requested item will be placed," + Environment.NewLine
                        + "then in the given order properties must be written delimited by '|' (Alt Gr + W)." + Environment.NewLine
                        + "For example if you want to create an int,string,string entity: 12|Value1|Value2" + Environment.NewLine
                        + "Not all properties have to be filled; int(ID),string,string: |Value1|Value2");
        }

        /// <summary>
        /// Collects all needed information for the feature and then execute it.
        /// </summary>
        public void Execute()
        {
            if (Menu.GetEntityType() is Type entityType)
            {
                IEntity entity = Activator.CreateInstance(entityType) as IEntity;

                Display.EntityProperties(entity);
                Console.WriteLine("Please enter the datas for the new entity as shown above delimited by |" +
                                Environment.NewLine + "Example: ID|Name|Etc...");

                if (Menu.GetEntityValues(entity) is string[] values)
                {
                    this.CreateEntity(entity, values);
                }
            }
        }

        private void CreateEntity(IEntity entity, string[] values)
        {
            int i = 0;
            foreach (PropertyInfo property in Menu.GetEntityProperties(entity.GetType()))
            {
                if (values[i] != null && !string.IsNullOrEmpty(values[i].ToString()))
                {
                    object value = Convert.ChangeType(values[i], property.PropertyType);
                    property.SetValue(entity, value, null);
                }

                i++;
            }

            try
            {
                Program.Logic.Create(entity);

                Display.ResultInfo(entity);
                Display.Pause("Created");
            }
            catch (DbEntityValidationException dbValidationEx)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var entityError in dbValidationEx.EntityValidationErrors)
                {
                    foreach (var validationError in entityError.ValidationErrors)
                    {
                        sb.AppendLine(string.Format("{0}: {1}", validationError.PropertyName, validationError.ErrorMessage));
                    }
                }

                Display.Pause(sb.ToString());
                this.Execute();
            }
            catch (Exception e)
            {
                Display.Pause(e.Message);
            }
        }
    }
}
