﻿// <copyright file="Join.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Console.Features
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using CarShop.Data;

    /// <summary>
    /// Feature which is able to join entity types extended with a where condition.
    /// </summary>
    [Feature("Join")]
    internal class Join : IFeature
    {
        /// <summary>
        /// Displays information about the feature
        /// </summary>
        public void Info()
        {
            Display.Pause("This feature is capable of joining entities." + Environment.NewLine
                        + "First you should select a table from where entities will be joined," + Environment.NewLine
                        + "then the other table which is to be connected to the first." + Environment.NewLine
                        + "For example in this database structure Model-Extra or Brand-Model" + Environment.NewLine
                        + "URL properties won't be displayed.");
        }

        /// <summary>
        /// Collects all needed information for the feature and then execute it.
        /// </summary>
        public void Execute()
        {
            if (Menu.GetEntityType() is Type entityType1)
            {
                if (Menu.GetWhereCondition(entityType1) is Predicate<IEntity> condition)
                {
                    if (Menu.GetEntityType() is Type entityType2)
                    {
                        if (Program.Logic.Join(entityType1, entityType2, condition) is IEnumerable<object> result)
                        {
                            Display.ResultInfo(result);
                            Display.Pause(result.Count() + " rows");
                        }
                        else
                        {
                            Display.Pause("There are no rows in this type of join");
                        }
                    }
                }
            }
        }
    }
}
