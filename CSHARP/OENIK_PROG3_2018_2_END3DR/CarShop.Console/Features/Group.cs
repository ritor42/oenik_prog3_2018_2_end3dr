﻿// <copyright file="Group.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Console.Features
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using CarShop.Data;

    /// <summary>
    /// Feature which is able to group entity types extended with a where condition.
    /// </summary>
    [Feature("Group")]
    internal class Group : IFeature
    {
        /// <summary>
        /// Displays information about the feature
        /// </summary>
        public void Info()
        {
            Display.Pause("This feature is capable of grouping entities." + Environment.NewLine
                        + "First you should select a table from where the requested item will be grouped," + Environment.NewLine
                        + "then enter the property's leftmost number to identify it for the application (for grouping).");
        }

        /// <summary>
        /// Collects all needed information for the feature and then execute it.
        /// </summary>
        public void Execute()
        {
            if (Menu.GetEntityType() is Type entityType)
            {
                if (Menu.GetWhereCondition(entityType) is Predicate<IEntity> condition)
                {
                    var outputOptions = Menu.GetEntityProperties(entityType);
                    var inputOptions = outputOptions.Select(x => x.Name);

                    void info() => Display.StringList(inputOptions);

                    if (Menu.GetFromInput(inputOptions, outputOptions, info) is PropertyInfo property)
                    {
                        if (Program.Logic.GroupBy(entityType, property, condition) is IEnumerable<object> result)
                        {
                            Display.ResultInfo(result);
                            Display.Pause(result.Count() + " rows");
                        }
                        else
                        {
                            Display.Pause("There are no rows in this type of join");
                        }
                    }
                }
            }
        }
    }
}
