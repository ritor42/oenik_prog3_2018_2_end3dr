﻿// <copyright file="Where.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Console.Features
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using CarShop.Data;

    /// <summary>
    /// Feature which is able to read specific entity type entities extended with a where condition.
    /// </summary>
    [Feature("Where")]
    internal class Where : IFeature
    {
        /// <summary>
        /// Displays information about the feature
        /// </summary>
        public void Info()
        {
            Display.Pause("This feature is capable of filtering entities." + Environment.NewLine
                        + "First you should select a table from where items will be filtered," + Environment.NewLine
                        + "then enter the property's leftmost number to identify it for the application." + Environment.NewLine
                        + "Finally specify the LIKE condition you would like to apply." + Environment.NewLine
                        + "For example if you want to filter by Compatibility," + Environment.NewLine
                        + "then select it, and enter the constraint: uni (it will filter by %uni%)");
        }

        /// <summary>
        /// Collects all needed information for the feature and then execute it.
        /// </summary>
        public void Execute()
        {
            if (Menu.GetEntityType() is Type entityType)
            {
                if (Menu.GetWhereCondition(entityType) is Predicate<IEntity> condition)
                {
                    if (Program.Logic.Where(entityType, condition) is IEnumerable<object> result)
                    {
                        Display.ResultInfo(result);
                        Display.Pause(result.Count() + " rows");
                    }
                    else
                    {
                        Display.Pause("There are no rows in this type of join");
                    }
                }
            }
        }
    }
}
