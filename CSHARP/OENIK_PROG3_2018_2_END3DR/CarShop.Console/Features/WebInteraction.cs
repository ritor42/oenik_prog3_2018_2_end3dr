﻿// <copyright file="WebInteraction.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Console.Features
{
    using System;
    using System.Net.Http;
    using System.Xml.Linq;

    /// <summary>
    /// Feature which is able to communicate with a Java web server.
    /// </summary>
    [Feature("Java/web interaction")]
    internal class WebInteraction : IFeature
    {
        /// <summary>
        /// Displays information about the feature
        /// </summary>
        public void Info()
        {
            Display.Pause("This feature is capable of carry out interaction with Java/web endpoint");
        }

        /// <summary>
        /// Collects all needed information for the feature and then execute it.
        /// </summary>
        public void Execute()
        {
            string name = string.Empty;
            string modelName = string.Empty;
            string price = string.Empty;

            Console.WriteLine("Please write your name:");
            if (!string.IsNullOrEmpty(name = Console.ReadLine()) && name != Menu.Back)
            {
                Console.WriteLine("Specify the car you want to buy:");
                if (!string.IsNullOrEmpty(modelName = Console.ReadLine()) && modelName != Menu.Back)
                {
                    Console.WriteLine("How much money you shall spend:");
                    if (!string.IsNullOrEmpty(price = Console.ReadLine()) && price != Menu.Back && int.TryParse(price, out int p))
                    {
                        Display.ResultInfo(Program.Logic.WebInteraction(name, modelName, p));
                        return;
                    }
                }
            }

            if (name != Menu.Back && modelName != Menu.Back && price != Menu.Back)
            {
                Console.WriteLine("You have given empty/null datas or invalid integer for the price.");
                this.Execute();
            }
        }
    }
}
