﻿// <copyright file="FeatureAttribute.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Console
{
    using System;

    /// <summary>
    /// Required attribute for all features which provides information for the Display.
    /// </summary>
    internal class FeatureAttribute : Attribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FeatureAttribute"/> class.
        /// </summary>
        /// <param name="title">Title of the feature</param>
        public FeatureAttribute(string title)
        {
            this.Title = title;
        }

        /// <summary>
        /// Gets title of the feature, can't be modified
        /// </summary>
        public string Title { get; private set; }
    }
}
