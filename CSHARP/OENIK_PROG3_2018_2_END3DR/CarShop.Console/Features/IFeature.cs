﻿// <copyright file="IFeature.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Console
{
    /// <summary>
    /// Required interface for all features to unify accessible methods
    /// </summary>
    internal interface IFeature
    {
        /// <summary>
        /// Displays information about the feature
        /// </summary>
        void Info();

        /// <summary>
        /// Collects all needed information for the feature and then execute it.
        /// </summary>
        void Execute();
    }
}
