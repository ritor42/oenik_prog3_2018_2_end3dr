﻿// <copyright file="Read.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Console.Features
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using CarShop.Data;

    /// <summary>
    /// Feature which is able to read specific entity type entities.
    /// </summary>
    [Feature("Read")]
    internal class Read : IFeature
    {
        /// <summary>
        /// Displays information about the feature
        /// </summary>
        public void Info()
        {
            Display.Pause("This feature is capable of reading entities." + Environment.NewLine
                        + "You should select a table from where entities will be displayed.");
        }

        /// <summary>
        /// Collects all needed information for the feature and then execute it.
        /// </summary>
        public void Execute()
        {
            if (Menu.GetEntityType() is Type entityType)
            {
                if (Menu.GetEntities(entityType) is IEnumerable<IEntity> entities)
                {
                    Display.ResultInfo(entities);
                    Display.Pause(entities.Count() + " rows");
                }
            }
        }
    }
}
