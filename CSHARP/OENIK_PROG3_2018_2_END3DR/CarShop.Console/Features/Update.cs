﻿// <copyright file="Update.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Console.Features
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using CarShop.Data;

    /// <summary>
    /// Feature which is able to update values of an entity.
    /// </summary>
    [Feature("Update")]
    internal class Update : IFeature
    {
        /// <summary>
        /// Displays information about the feature
        /// </summary>
        public void Info()
        {
            Display.Pause("This feature is capable of updating entities." + Environment.NewLine
                        + "First you should select a table from where the requested item will be updated," + Environment.NewLine
                        + "then enter the item's leftmost number to identify it for the application." + Environment.NewLine
                        + "Finally properties in the given order must be written delimited by '|' (Alt Gr + W)." + Environment.NewLine
                        + "For example if you want to update an int,string,string entity: 12|Value1|Value2" + Environment.NewLine
                        + "Not all properties have to be filled; int(ID),string,string,int: |Value1|Value2|");
        }

        /// <summary>
        /// Collects all needed information for the feature and then execute it.
        /// </summary>
        public void Execute()
        {
            if (Menu.GetEntityType() is Type entityType)
            {
                if (Menu.GetEntities(entityType) is IEnumerable<IEntity> entities)
                {
                    if (Menu.GetEntity(entities) is IEntity entity)
                    {
                        Console.WriteLine("Please enter the datas for the updated entity as shown above delimited by |" + Environment.NewLine
                                        + "Example: ID|Name|Etc..." + Environment.NewLine
                                        + "If you don't want to modify a field, then pass it by ||");

                        if (Menu.GetEntityValues(entity) is string[] values)
                        {
                            UpdateEntity(entity, values);
                        }
                    }
                }
            }
        }

        private static void UpdateEntity(IEntity entity, string[] values)
        {
            int i = 0;
            foreach (PropertyInfo property in Menu.GetEntityProperties(entity.GetType()))
            {
                if (values[i] != null && !string.IsNullOrEmpty(values[i].ToString()))
                {
                    object value = Convert.ChangeType(values[i], property.PropertyType);
                    property.SetValue(entity, value, null);
                }

                i++;
            }

            try
            {
                Program.Logic.Update(entity);
                Display.ResultInfo(entity);
                Display.Pause("Updated");
            }
            catch (Exception e)
            {
                Display.Pause(e.Message);
            }
        }
    }
}
