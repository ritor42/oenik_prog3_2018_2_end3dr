﻿// <copyright file="Delete.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Console.Features
{
    using System;
    using System.Collections.Generic;
    using CarShop.Data;

    /// <summary>
    /// Feature which is able to delete specific entity from any entity type.
    /// </summary>
    [Feature("Delete")]
    internal class Delete : IFeature
    {
        /// <summary>
        /// Displays information about the feature
        /// </summary>
        public void Info()
        {
            Display.Pause("This feature is capable of deleting entities." + Environment.NewLine
                        + "First you should select a table from where the requested item will be removed," + Environment.NewLine
                        + "then enter the item's leftmost number to identify it for the application.");
        }

        /// <summary>
        /// Collects all needed information for the feature and then execute it.
        /// </summary>
        public void Execute()
        {
            if (Menu.GetEntityType() is Type entityType)
            {
                if (Menu.GetEntities(entityType) is IEnumerable<IEntity> entities)
                {
                    if (Menu.GetEntity(entities) is IEntity entity)
                    {
                        try
                        {
                            Program.Logic.Delete(entity);
                            Display.ResultInfo(entity);
                            Display.Pause("Deleted");
                        }
                        catch (Exception e)
                        {
                            Display.Pause(e.Message);
                        }
                    }
                }
            }
        }
    }
}
