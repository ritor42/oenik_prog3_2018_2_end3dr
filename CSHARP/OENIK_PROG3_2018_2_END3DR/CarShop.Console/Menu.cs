﻿// <copyright file="Menu.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Console
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using CarShop.Data;

    /// <summary>
    /// Controls menu system by input/output management
    /// </summary>
    internal static class Menu
    {
        /// <summary>
        /// String constant for being able to identify when the user wants to step back.
        /// </summary>
        internal const string Back = "back";

        /// <summary>
        /// Provides information about service-based database entity types
        /// </summary>
        internal static readonly IEnumerable<Type> EntityTypes = Assembly.Load("CarShop.Data").GetTypes().Where(x => typeof(IEntity).IsAssignableFrom(x) && x.IsClass);
        private static readonly Predicate<string> StepBack = input => EqualsInvariant(Back, input);
        private static readonly SortedDictionary<string, IFeature> Features = new SortedDictionary<string, IFeature>();

        /// <summary>
        /// Starts menu system
        /// </summary>
        internal static void Start()
        {
            var assemblies = Assembly.GetExecutingAssembly().GetTypes()
                                     .Where(x => typeof(IFeature).IsAssignableFrom(x) && x.IsDefined(typeof(FeatureAttribute)));

            foreach (Type assembly in assemblies)
            {
                string key = assembly.GetCustomAttribute<FeatureAttribute>().Title;
                IFeature value = Activator.CreateInstance(assembly) as IFeature;

                if (!Features.ContainsKey(key))
                {
                    Features.Add(key, value);
                }
            }

            SelectFeature();
        }

        /// <summary>
        /// Gets user selected entity type
        /// </summary>
        /// <returns>Entity type</returns>
        internal static Type GetEntityType()
        {
            Display.EntityTypes();

            var outputOptions = EntityTypes;
            var inputOptions = EntityTypes.Select(x => x.Name);

            return GetFromInput(inputOptions, outputOptions, null);
        }

        /// <summary>
        /// Gets all entities for the given type from business logic
        /// </summary>
        /// <param name="entityType">Entity type</param>
        /// <returns>Collection of same type of entities</returns>
        internal static IEnumerable<IEntity> GetEntities(Type entityType)
        {
            if (Program.Logic.Read(entityType) is IEnumerable<IEntity> entities)
            {
                if (entities.Count() > 0)
                {
                    return entities;
                }
                else
                {
                    Display.Pause("There are no rows in this table");
                }
            }

            return null;
        }

        /// <summary>
        /// Gets user selected entity from the given collection
        /// </summary>
        /// <param name="entities">Collection of same type of entities</param>
        /// <returns>Selected entity</returns>
        internal static IEntity GetEntity(IEnumerable<IEntity> entities)
        {
            Display.ResultInfo(entities);

            var outputOptions = entities;
            var inputOptions = Enumerable.Range(1, entities.Count()).Select(x => x.ToString());

            return GetFromInput(inputOptions, outputOptions, null);
        }

        /// <summary>
        /// Gets user given values of a specific entity type
        /// </summary>
        /// <param name="entity">Specific entity</param>
        /// <returns>Correct number of entity property values</returns>
        internal static string[] GetEntityValues(IEntity entity)
        {
            PropertyInfo[] properties = GetEntityProperties(entity.GetType()).ToArray();

            string input = string.Empty;
            while (!StepBack(input) && input.Split('|').Length != properties.Length)
            {
                input = Console.ReadLine();
            }

            if (!StepBack(input))
            {
                return input.Split('|');
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Gets user given T object from specific output options by selecting an input option
        /// </summary>
        /// <typeparam name="T">Generic type for high flexibility</typeparam>
        /// <param name="inputOptions">Collection of input options with N length</param>
        /// <param name="outputOptions">Collection of output options with N length</param>
        /// <param name="info"> Action for providing information for the user.</param>
        /// <returns>Selected entity from output options</returns>
        internal static T GetFromInput<T>(IEnumerable<string> inputOptions, IEnumerable<T> outputOptions, Action info)
        {
            var inputOptionsList = inputOptions.ToList();
            var outputOptionsList = outputOptions.ToList();

            string input = string.Empty;
            while (!StepBack(input))
            {
                info?.Invoke();
                input = Console.ReadLine();
                if (inputOptionsList.FindIndex(x => EqualsInvariant(x, input)) is int index && index != -1)
                {
                    return outputOptionsList[index];
                }
                else if (int.TryParse(input, out index) && outputOptions.Count() >= index && index > 0)
                {
                    return outputOptionsList[index - 1];
                }
            }

            return default(T);
        }

        /// <summary>
        /// Gets user given where condition for specific column with like - ordinal ignore value.
        /// </summary>
        /// <param name="entityType">Entity type</param>
        /// <returns>Predicate for entities which decides which entities shall be returned</returns>
        internal static Predicate<IEntity> GetWhereCondition(Type entityType)
        {
            var inputNeeded = new string[2] { "True", "False" };
            var outputNeeded = new bool[2] { true, false };

            void neededInfo()
            {
                Console.WriteLine("Do you want to add where condition?");
                Display.StringList(inputNeeded);
            }

            if (Menu.GetFromInput(inputNeeded, outputNeeded, neededInfo))
            {
                var outputOptions = Menu.GetEntityProperties(entityType);
                var inputOptions = outputOptions.Select(x => x.Name);

                void info() => Display.StringList(inputOptions);

                if (Menu.GetFromInput(inputOptions, outputOptions, info) is PropertyInfo property)
                {
                    var constraint = Console.ReadLine();
                    Predicate<IEntity> condition = entity =>
                    {
                        object value = property.GetValue(entity);
                        if (value != null)
                        {
                            return value.ToString().ToLower().Contains(constraint.ToLower());
                        }
                        else
                        {
                            return true;
                        }
                    };

                    return condition;
                }
            }
            else
            {
                return (entity) => true;
            }

            return null;
        }

        /// <summary>
        /// Gets collection of propertyinfo which are only columns in the table
        /// </summary>
        /// <param name="type">Entity type</param>
        /// <returns>Collection of propertyinfo of table columns</returns>
        internal static IEnumerable<PropertyInfo> GetEntityProperties(Type type) => type.GetProperties().Where(x => !x.PropertyType.IsGenericType && !typeof(IEntity).IsAssignableFrom(x.PropertyType));

        private static bool EqualsInvariant(string input1, string input2) => input1.Equals(input2, StringComparison.OrdinalIgnoreCase);

        private static void SelectFeature()
        {
            var outputOptions = Features.Values;
            var inputOptions = Features.Keys;

            void info() => Display.Features(inputOptions);

            while (GetFromInput(inputOptions, outputOptions, info) is IFeature feature)
            {
                SelectMethod(feature);
            }
        }

        private static void SelectMethod(IFeature feature)
        {
            var outputOptions = feature.GetType().GetInterfaceMap(typeof(IFeature)).InterfaceMethods;
            var inputOptions = outputOptions.Select(x => x.Name);

            void info() => Display.FeatureInfo(feature, inputOptions);

            while (GetFromInput(inputOptions, outputOptions, info) is MethodInfo method)
            {
                try
                {
                    method.Invoke(feature, null);
                }
                catch (TargetInvocationException)
                {
                    Display.Pause("Ez még nincs kész");
                }
            }
        }
    }
}
