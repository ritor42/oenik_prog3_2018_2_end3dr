﻿// <copyright file="Display.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Console
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Xml.Linq;
    using CarShop.Data;

    /// <summary>
    /// Controls output for the application
    /// </summary>
    internal static class Display
    {
        private const string Back = "If you want to step back enter back. \r\n" +
                                    "Please specify one of them";

        /// <summary>
        /// Provides given information for the user, needed key press for continure
        /// </summary>
        /// <param name="message">Given information to display</param>
        internal static void Pause(string message = "")
        {
            if (!string.IsNullOrEmpty(message))
            {
                Console.WriteLine(message);
            }

            Console.WriteLine("Press a key to continue");
            Console.ReadKey(true);
        }

        /// <summary>
        /// Displays all available features
        /// </summary>
        /// <param name="keys">Given collection of feature names</param>
        internal static void Features(IEnumerable<string> keys)
        {
            Console.Clear();

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("CarShop features:");
            Listing(keys, ref sb);
            sb.AppendLine(Back);

            Console.WriteLine(sb.ToString());
        }

        /// <summary>
        /// Displays basic information about a specific selected feature
        /// </summary>
        /// <param name="feature">Given selected feature</param>
        /// <param name="methods">Given collection of available methods of the feature</param>
        internal static void FeatureInfo(IFeature feature, IEnumerable<string> methods)
        {
            Console.Clear();

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Feature: " + feature.ToString());
            sb.AppendLine("Accessible methods:");
            Listing(methods, ref sb);
            sb.AppendLine(Back);

            Console.WriteLine(sb.ToString());
        }

        /// <summary>
        /// Displays result information: Entity/Collection of entity/Collection of collection of entity (Join)
        /// </summary>
        /// <typeparam name="T">Generic type for high flexibility</typeparam>
        /// <param name="result">Result of a feature</param>
        internal static void ResultInfo<T>(T result)
        {
            int rowNum;
            int columnNum;
            Func<int, string> title;
            Func<int, int, object> value;

            if (result is XDocument doc)
            {
                XElement[] elements = doc.Descendants("offer").ToArray();
                string[] properties = elements.Elements().Select(x => x.Name.LocalName).Distinct().ToArray();

                rowNum = elements.Length + 1;
                columnNum = properties.Length;

                title = index => properties[index];
                value = (y, x) => elements[y].Element(properties[x]).Value;

                Pause(TableTab(rowNum, columnNum, title, value));
            }
            else if (result is IEntity || (result is IEnumerable<IEntity> ien && ien.Count() > 0))
            {
                IEntity entity = result as IEntity;
                IEnumerable<IEntity> entities = result as IEnumerable<IEntity>;

                Type entityType = (entity != null) ? entity.GetType() : entities.First().GetType();
                PropertyInfo[] properties = Menu.GetEntityProperties(entityType).ToArray();

                var items = (entities != null) ? entities.ToArray() : new object[] { entity };

                rowNum = items.Length + 1;
                columnNum = properties.Length;
                title = index => properties[index].Name;
                value = (y, x) => properties[x].GetValue(items[y]);

                Console.WriteLine(TableTab(rowNum, columnNum, title, value));
            }
            else if (result is IEnumerable<object> objects && objects.Count() > 0)
            {
                var groups = objects.ToArray();
                PropertyInfo[] groupProps = groups[0].GetType().GetProperties();

                int[] itemPropsCount = new int[groupProps.Length];
                PropertyInfo[][] itemProps = new PropertyInfo[groupProps.Length][];

                var items = new object[groups.Length, groupProps.Length];

                for (int x = 0; x < items.GetLength(1); x++)
                {
                    for (int y = 0; y < items.GetLength(0); y++)
                    {
                        items[y, x] = groupProps[x].GetValue(groups[y]);
                    }

                    itemProps[x] = Menu.GetEntityProperties(items[0, x].GetType()).Where(z => z.Name != "URL").ToArray();
                    itemPropsCount[x] = itemProps[x].Length;
                }

                rowNum = groups.Length;
                columnNum = itemPropsCount.Sum();

                title = index =>
                {
                    int i = 0;

                    while (index >= itemPropsCount[i])
                    {
                        index -= itemPropsCount[i];
                        i++;
                    }

                    return itemProps[i][index].Name;
                };

                value = (y, x) =>
                {
                    int i = 0;

                    while (x >= itemPropsCount[i])
                    {
                        x -= itemPropsCount[i];
                        i++;
                    }

                    return itemProps[i][x].GetValue(items[y, i]);
                };

                Console.WriteLine(TableTab(rowNum + 1, columnNum, title, value));
            }
        }

        /// <summary>
        /// Displays entity types
        /// </summary>
        internal static void EntityTypes()
        {
            StringBuilder sb = new StringBuilder();
            Listing(Menu.EntityTypes.Select(x => x.Name), ref sb);

            Console.WriteLine(sb.ToString());
        }

        /// <summary>
        /// Displays column propertyinfos of a given entity
        /// </summary>
        /// <param name="entity">Given entity</param>
        internal static void EntityProperties(IEntity entity)
        {
            if (entity != null)
            {
                IEnumerable<PropertyInfo> properties = Menu.GetEntityProperties(entity.GetType());

                StringBuilder sb = new StringBuilder();
                foreach (PropertyInfo property in properties)
                {
                    sb.AppendLine(string.Format("\t\t {0}: {1} {2}", property.Name, Filler(50 - property.Name.Length), property.PropertyType));
                }

                Console.WriteLine(sb.ToString());
            }
        }

        /// <summary>
        /// Displays a collection of string in list format
        /// </summary>
        /// <param name="list">Given collection of string</param>
        internal static void StringList(IEnumerable<string> list)
        {
            StringBuilder sb = new StringBuilder();
            Listing(list, ref sb);
            Console.WriteLine(sb.ToString());
        }

        private static void Listing(IEnumerable<string> list, ref StringBuilder sb)
        {
            int i = 1;
            foreach (string item in list)
            {
                sb.AppendLine(string.Format("\t {0}) \t {1}", i, item));
                i++;
            }
        }

        private static string TableTab(int rowNum, int columnNum, Func<int, string> title, Func<int, int, object> itemValue)
        {
            var spaces = new int[columnNum];
            var matrix = new string[rowNum, columnNum];

            for (int x = 0; x < columnNum; x++)
            {
                string propertyName = title(x);

                matrix[0, x] = propertyName;
                int max = propertyName.Length;
                for (int y = 1; y < rowNum; y++)
                {
                    object propValue = itemValue(y - 1, x);
                    string value = (propValue == null) ? string.Empty : propValue.ToString();
                    max = (max > value.Length) ? max : value.Length;
                    matrix[y, x] = value;
                }

                spaces[x] = max + 3;
            }

            return TableTabSB(matrix, spaces);
        }

        private static string TableTabSB(string[,] matrix, int[] spaces)
        {
            int rowNum = matrix.GetLength(0);
            int columnNum = matrix.GetLength(1);

            StringBuilder sb = new StringBuilder();

            sb.Append("\t\t");
            for (int x = 0; x < columnNum; x++)
            {
                sb.Append(matrix[0, x] + Filler(spaces[x] - matrix[0, x].Length, ' '));
            }

            sb.AppendLine();

            for (int y = 1; y < rowNum; y++)
            {
                sb.Append(string.Format("\t {0}) \t", y));
                for (int x = 0; x < columnNum; x++)
                {
                    sb.Append(matrix[y, x] + Filler(spaces[x] - matrix[y, x].Length, ' '));
                }

                sb.AppendLine();
            }

            return sb.ToString();
        }

        private static string Filler(int length, char filler = '.')
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < length; i++)
            {
                sb.Append(filler);
            }

            return sb.ToString();
        }
    }
}
