﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Console
{
    using CarShop.Logic;

    /// <summary>
    /// Console application main entry
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// Business logic entity for unified access
        /// </summary>
        public static readonly ILogic Logic = new Logic();

        private static void Main(string[] args)
        {
            Menu.Start();
        }
    }
}
