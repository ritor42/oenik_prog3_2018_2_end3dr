﻿// <copyright file="LogicTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml.Linq;
    using CarShop.Data;
    using CarShop.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Unit testing for Logic
    /// </summary>
    [TestFixture]
    public class LogicTests
    {
        private ILogic logic;

        /// <summary>
        /// Setup for unit testing
        /// </summary>
        [SetUp]
        public void Setup()
        {
            IEnumerable<IEntity> def = null;
            IEnumerable<IEntity> empty = new List<IEntity>();

            Mock<IRepository> mocking = new Mock<IRepository>();
            mocking.Setup(x => x.Read(typeof(Model))).Returns(empty);
            mocking.Setup(x => x.Read(typeof(Brand))).Returns(empty);
            mocking.Setup(x => x.Read(typeof(Extra))).Returns(empty);
            mocking.Setup(x => x.Read(null)).Returns(def);

            this.logic = new Logic(mocking.Object);
        }

        /// <summary>
        /// Unit test for Logic -> null check
        /// </summary>
        [Test]
        public void Logic()
        {
            Assert.That(() => new Logic(null), Throws.ArgumentNullException);
        }

        /// <summary>
        /// Unit test for Logic create method -> null check
        /// </summary>
        [Test]
        public void Logic_Create_Null()
        {
            IEntity entity = null;

            void act(IEntity e) => this.logic.Create(e);

            Assert.That(() => act(entity), Throws.Nothing);
        }

        /// <summary>
        /// Unit test for Logic create method -> fake objects check
        /// </summary>
        [Test]
        public void Logic_Create_Fake()
        {
            IEntity entity = new Brand();

            void act(IEntity e) => this.logic.Create(e);

            Assert.That(() => act(entity), Throws.Nothing);
        }

        /// <summary>
        /// Unit test for Logic delete method -> null check
        /// </summary>
        [Test]
        public void Logic_Delete_Null()
        {
            IEntity entity = null;

            void act(IEntity e) => this.logic.Delete(e);

            Assert.That(() => act(entity), Throws.Nothing);
        }

        /// <summary>
        /// Unit test for Logic delete method -> fake objects check
        /// </summary>
        [Test]
        public void Logic_Delete_Fake()
        {
            IEntity entity = new Model();

            void act(IEntity e) => this.logic.Delete(e);

            Assert.That(() => act(entity), Throws.Nothing);
        }

        /// <summary>
        /// Unit test for Logic update method -> null check
        /// </summary>
        [Test]
        public void Logic_Update_Null()
        {
            IEntity entity = null;

            void act(IEntity e) => this.logic.Update(e);

            Assert.That(() => act(entity), Throws.Nothing);
        }

        /// <summary>
        /// Unit test for Logic update method -> fake objects check
        /// </summary>
        [Test]
        public void Logic_Update_Fake()
        {
            IEntity entity = new Extra();

            void act(IEntity e) => this.logic.Update(e);

            Assert.That(() => act(entity), Throws.Nothing);
        }

        /// <summary>
        /// Unit test for Logic read method -> null check
        /// </summary>
        [Test]
        public void Logic_Read_Null()
        {
            Type entityType = null;

            IEnumerable<IEntity> act(Type t) => this.logic.Read(t);

            Assert.That(() => act(entityType), Throws.Nothing);
            Assert.That(() => act(entityType), Is.EqualTo(null));
        }

        /// <summary>
        /// Unit test for Logic read method -> fake objects check
        /// </summary>
        /// <param name="entityType">Entity Type</param>
        [Test]
        [Sequential]
        public void Logic_Read_Fake([Values(typeof(Model), typeof(Brand), typeof(Extra))] Type entityType)
        {
            IEnumerable<IEntity> act(Type t) => this.logic.Read(t);

            Assert.That(() => act(entityType), Throws.Nothing);
            Assert.That(() => act(entityType), Is.Not.EqualTo(null));
            Assert.That(() => act(entityType).Count(), Is.EqualTo(0));
        }

        /// <summary>
        /// Unit test for Logic join method -> null check
        /// </summary>
        [Test]
        public void Logic_Join_Null()
        {
            Type mainEntity = null;
            Type subEntity = null;
            Predicate<IEntity> condition = null;

            IEnumerable<object> act(Type m, Type s, Predicate<IEntity> c) => this.logic.Join(m, s, c);

            Assert.That(() => act(mainEntity, subEntity, condition), Throws.Nothing);
            Assert.That(() => act(mainEntity, subEntity, condition), Is.EqualTo(null));
        }

        /// <summary>
        /// Unit test for Logic join method -> fake objects check
        /// </summary>
        [Test]
        public void Logic_Join_Fake()
        {
            Type mainEntity = typeof(Brand);
            Type subEntity = typeof(Model);
            bool condition(IEntity entity) => true;

            IEnumerable<object> act(Type m, Type s, Predicate<IEntity> c) => this.logic.Join(m, s, c);

            Assert.That(() => act(mainEntity, subEntity, condition), Throws.Nothing);
            Assert.That(() => act(mainEntity, subEntity, condition), Is.Not.EqualTo(null));
            Assert.That(() => act(mainEntity, subEntity, condition).Count(), Is.EqualTo(0));
        }

        /// <summary>
        /// Unit test for Logic groupbyJoin method -> null check
        /// </summary>
        [Test]
        public void Logic_GroupByJoin_Null()
        {
            Type mainEntity = null;
            Type subEntity = null;
            Predicate<IEntity> condition = null;

            IEnumerable<object> act(Type m, Type s, Predicate<IEntity> c) => this.logic.GroupByJoin(m, s, c);

            Assert.That(() => act(mainEntity, subEntity, condition), Throws.Nothing);
            Assert.That(() => act(mainEntity, subEntity, condition), Is.EqualTo(null));
        }

        /// <summary>
        /// Unit test for Logic groupbyJoin method -> fake objects check
        /// </summary>
        [Test]
        public void Logic_GroupByJoin_Fake()
        {
            Type mainEntity = typeof(Brand);
            Type subEntity = typeof(Model);
            bool condition(IEntity entity) => true;

            IEnumerable<object> act(Type m, Type s, Predicate<IEntity> c) => this.logic.GroupByJoin(m, s, c);

            Assert.That(() => act(mainEntity, subEntity, condition), Throws.Nothing);
            Assert.That(() => act(mainEntity, subEntity, condition), Is.EqualTo(null));
        }

        /// <summary>
        /// Unit test for Logic groupby method -> null check
        /// </summary>
        [Test]
        public void Logic_GroupBy_Null()
        {
            Type entityType = null;
            PropertyInfo property = null;
            Predicate<IEntity> condition = null;

            IEnumerable<object> act(Type e, PropertyInfo p, Predicate<IEntity> c) => this.logic.GroupBy(e, p, c);

            Assert.That(() => act(entityType, property, condition), Throws.Nothing);
            Assert.That(() => act(entityType, property, condition), Is.EqualTo(null));
        }

        /// <summary>
        /// Unit test for Logic groupby method -> fake objects check
        /// </summary>
        [Test]
        public void Logic_GroupBy_Fake()
        {
            Type entityType = typeof(Brand);
            PropertyInfo property = typeof(Brand).GetProperties()[0];
            bool condition(IEntity entity) => true;

            IEnumerable<object> act(Type e, PropertyInfo p, Predicate<IEntity> c) => this.logic.GroupBy(e, p, c);

            Assert.That(() => act(entityType, property, condition), Throws.Nothing);
            Assert.That(() => act(entityType, property, condition), Is.Not.EqualTo(null));
            Assert.That(() => act(entityType, property, condition).Count(), Is.EqualTo(0));
        }

        /// <summary>
        /// Unit test for Logic create method -> null check
        /// </summary>
        [Test]
        public void Logic_Where_Null()
        {
            Type entityType = null;
            Predicate<IEntity> condition = null;

            IEnumerable<object> act(Type e, Predicate<IEntity> c) => this.logic.Where(e, c);

            Assert.That(() => act(entityType, condition), Throws.Nothing);
            Assert.That(() => act(entityType, condition), Is.EqualTo(null));
        }

        /// <summary>
        /// Unit test for Logic where method -> fake objects check
        /// </summary>
        [Test]
        public void Logic_Where_Fake()
        {
            Type entityType = typeof(Brand);
            bool condition(IEntity entity) => true;

            IEnumerable<object> act(Type e, Predicate<IEntity> c) => this.logic.Where(e, c);

            Assert.That(() => act(entityType, condition), Throws.Nothing);
            Assert.That(() => act(entityType, condition), Is.Not.EqualTo(null));
            Assert.That(() => act(entityType, condition).Count(), Is.EqualTo(0));
        }

        /// <summary>
        /// Unit test for Logic WebInteraction method -> null check
        /// </summary>
        [Test]
        public void Logic_WebInteraction_Null()
        {
            string value = null;

            XDocument act(string n, string m, int p) => this.logic.WebInteraction(n, m, p);

            Assert.That(() => act(value, value, 0), Throws.Nothing);
            Assert.That(() => act(value, value, 0), Is.EqualTo(null));
        }

        /// <summary>
        /// Unit test for Logic WebInteraction method -> fake objects check
        /// </summary>
        [Test]
        public void Logic_WebInteraction_Fake()
        {
            string name = "Bence";
            string modelName = "Volkswagen Blue Polo GT";

            XDocument act(string n, string m, int p) => this.logic.WebInteraction(n, m, p);

            Assert.That(() => act(name, modelName, 0), Throws.Nothing);
            Assert.That(() => act(name, modelName, 0), Is.Not.EqualTo(null));
        }
    }
}
