﻿// <copyright file="TestRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CarShop.Data;
    using CarShop.Repository;

    /// <summary>
    /// Repository which implements IRepository to be able to test Logic
    /// </summary>
    internal class TestRepository : IRepository
    {
        /// <summary>
        /// Does nothing
        /// </summary>
        /// <param name="entity">Entity</param>
        public void Create(IEntity entity)
        {
        }

        /// <summary>
        /// Does nothing
        /// </summary>
        /// <param name="entity">Entity</param>
        public void Delete(IEntity entity)
        {
        }

        /// <summary>
        /// Does nothing
        /// </summary>
        /// <param name="entity">Entity</param>
        public void Update(IEntity entity)
        {
        }

        /// <summary>
        /// Returns null
        /// </summary>
        /// <param name="entityType">Type of dBSet</param>
        /// <returns>null</returns>
        public IEnumerable<IEntity> Read(Type entityType)
        {
            return null;
        }
    }
}
