var class_car_shop_1_1_data_1_1_model =
[
    [ "Model", "class_car_shop_1_1_data_1_1_model.html#af81b23077c51e4ed4de428a7a463ee52", null ],
    [ "Brand", "class_car_shop_1_1_data_1_1_model.html#add21c73639db2e06006b88fb3f8e13ef", null ],
    [ "BrandID", "class_car_shop_1_1_data_1_1_model.html#ad56221568eb77f07f989a8c89690e2e7", null ],
    [ "EngineConfiguration", "class_car_shop_1_1_data_1_1_model.html#aa597dff10bb9811e937baf08631ae141", null ],
    [ "EngineType", "class_car_shop_1_1_data_1_1_model.html#a69512ccbea58b505f3f0938c5a9b347b", null ],
    [ "Extra", "class_car_shop_1_1_data_1_1_model.html#a4d0175ab2ddbd85823864053f89a7ad1", null ],
    [ "HorsePower", "class_car_shop_1_1_data_1_1_model.html#aba3d2f2cee38cac6dcdca5b952ce0116", null ],
    [ "ModelID", "class_car_shop_1_1_data_1_1_model.html#a72e44f3adfb4ee5d37e8cd7cba17ec68", null ],
    [ "Name", "class_car_shop_1_1_data_1_1_model.html#acce4fc388cd4dfeff7d123b7c52742a1", null ],
    [ "ReleaseYear", "class_car_shop_1_1_data_1_1_model.html#aa5c2505a346186784f5f53da97b8dc00", null ],
    [ "TankCapacity", "class_car_shop_1_1_data_1_1_model.html#ade23b9279ecfe53df697dca743de3078", null ],
    [ "TopSpeed", "class_car_shop_1_1_data_1_1_model.html#aea098da8feb4d78d3382ea5a4190b72a", null ],
    [ "URL", "class_car_shop_1_1_data_1_1_model.html#a11d81f13fa272771382abba79ae4ec4c", null ],
    [ "Weight", "class_car_shop_1_1_data_1_1_model.html#af552b1fa669e7210ffdf55343a30d896", null ]
];