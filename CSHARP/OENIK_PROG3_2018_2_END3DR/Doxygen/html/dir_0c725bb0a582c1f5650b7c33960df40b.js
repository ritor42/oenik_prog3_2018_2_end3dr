var dir_0c725bb0a582c1f5650b7c33960df40b =
[
    [ "CarShop.Console", "dir_053cc587f7cd92566d07b7d9f820fde6.html", "dir_053cc587f7cd92566d07b7d9f820fde6" ],
    [ "CarShop.Data", "dir_8ce5ee0f2bb0ac768efcf1e0a4288c19.html", "dir_8ce5ee0f2bb0ac768efcf1e0a4288c19" ],
    [ "CarShop.Logic", "dir_a87796fef6f90a07396626001d21a848.html", "dir_a87796fef6f90a07396626001d21a848" ],
    [ "CarShop.Logic.Tests", "dir_6d4041dcd02074210393583bd1c761e0.html", "dir_6d4041dcd02074210393583bd1c761e0" ],
    [ "CarShop.Repository", "dir_08cd7a57d28f13011293a6371077844b.html", "dir_08cd7a57d28f13011293a6371077844b" ],
    [ "CarShop.Repository.Tests", "dir_b829de081219c5bc1f4f0dbbdbdb4ac9.html", "dir_b829de081219c5bc1f4f0dbbdbdb4ac9" ]
];