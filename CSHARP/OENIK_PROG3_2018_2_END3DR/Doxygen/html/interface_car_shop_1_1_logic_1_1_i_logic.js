var interface_car_shop_1_1_logic_1_1_i_logic =
[
    [ "Create", "interface_car_shop_1_1_logic_1_1_i_logic.html#a5b0fb9abfb887fea87031389d6dd692e", null ],
    [ "Delete", "interface_car_shop_1_1_logic_1_1_i_logic.html#a81f219427306d9428a9adbc445282515", null ],
    [ "GroupBy", "interface_car_shop_1_1_logic_1_1_i_logic.html#ab13c4dc91d941c8ecdf87e8bc619184d", null ],
    [ "GroupByJoin", "interface_car_shop_1_1_logic_1_1_i_logic.html#a0049697eb55f13ca2024c226ea0c413b", null ],
    [ "Join", "interface_car_shop_1_1_logic_1_1_i_logic.html#ae0dd7956e83d9e06568ca2ea9010dba7", null ],
    [ "Read", "interface_car_shop_1_1_logic_1_1_i_logic.html#a628a6caf82a32f46094c40e1c6ee89f8", null ],
    [ "Update", "interface_car_shop_1_1_logic_1_1_i_logic.html#af53df6d2269b1c051d6e159c583f4644", null ],
    [ "WebInteraction", "interface_car_shop_1_1_logic_1_1_i_logic.html#a91652d8529027984ecb838d568bae0d3", null ],
    [ "Where", "interface_car_shop_1_1_logic_1_1_i_logic.html#aceedc68633cdb0a3243ee7d15b2fd508", null ]
];