var hierarchy =
[
    [ "DbContext", null, [
      [ "CarShop.Data.DBEntities", "class_car_shop_1_1_data_1_1_d_b_entities.html", null ]
    ] ],
    [ "CarShop.Data.IEntity", "interface_car_shop_1_1_data_1_1_i_entity.html", [
      [ "CarShop.Data.Brand", "class_car_shop_1_1_data_1_1_brand.html", null ],
      [ "CarShop.Data.Extra", "class_car_shop_1_1_data_1_1_extra.html", null ],
      [ "CarShop.Data.Model", "class_car_shop_1_1_data_1_1_model.html", null ]
    ] ],
    [ "CarShop.Logic.ILogic", "interface_car_shop_1_1_logic_1_1_i_logic.html", [
      [ "CarShop.Logic.Logic", "class_car_shop_1_1_logic_1_1_logic.html", null ]
    ] ],
    [ "CarShop.Repository.IRepository", "interface_car_shop_1_1_repository_1_1_i_repository.html", [
      [ "CarShop.Repository.Repository", "class_car_shop_1_1_repository_1_1_repository.html", null ]
    ] ],
    [ "CarShop.Logic.Tests.LogicTests", "class_car_shop_1_1_logic_1_1_tests_1_1_logic_tests.html", null ],
    [ "CarShop.Repository.Tests.RepositoryTests", "class_car_shop_1_1_repository_1_1_tests_1_1_repository_tests.html", null ]
];