var searchData=
[
  ['read',['Read',['../interface_car_shop_1_1_logic_1_1_i_logic.html#a628a6caf82a32f46094c40e1c6ee89f8',1,'CarShop.Logic.ILogic.Read()'],['../class_car_shop_1_1_logic_1_1_logic.html#ab9f0e025ffd240708793adba2843f7ab',1,'CarShop.Logic.Logic.Read()'],['../interface_car_shop_1_1_repository_1_1_i_repository.html#a4b3e200945cc04a24adf39d26fa17fe1',1,'CarShop.Repository.IRepository.Read()'],['../class_car_shop_1_1_repository_1_1_repository.html#aef3992cbddf32bfce12dd4826246c6fc',1,'CarShop.Repository.Repository.Read()']]],
  ['repository',['Repository',['../class_car_shop_1_1_repository_1_1_repository.html#a33e85f204eb76f1de043e32d91dc4acc',1,'CarShop.Repository.Repository.Repository()'],['../class_car_shop_1_1_repository_1_1_repository.html#aa4a842af481b970253c600a36c35ca1b',1,'CarShop.Repository.Repository.Repository(DBEntities dB)']]],
  ['repository_5fcreate_5ffake',['Repository_Create_Fake',['../class_car_shop_1_1_repository_1_1_tests_1_1_repository_tests.html#ad28549c723a75b60acb7097f863b19af',1,'CarShop::Repository::Tests::RepositoryTests']]],
  ['repository_5fcreate_5fnull',['Repository_Create_Null',['../class_car_shop_1_1_repository_1_1_tests_1_1_repository_tests.html#a98a4500bc6ce48b648ecbea08f2095b2',1,'CarShop::Repository::Tests::RepositoryTests']]],
  ['repository_5fdelete_5ffake',['Repository_Delete_Fake',['../class_car_shop_1_1_repository_1_1_tests_1_1_repository_tests.html#ad656749e2010abb609520f6af947c5af',1,'CarShop::Repository::Tests::RepositoryTests']]],
  ['repository_5fdelete_5fnull',['Repository_Delete_Null',['../class_car_shop_1_1_repository_1_1_tests_1_1_repository_tests.html#a2d3a659e1ee5d19d45f4d9509a2653b9',1,'CarShop::Repository::Tests::RepositoryTests']]],
  ['repository_5fread_5ffake',['Repository_Read_Fake',['../class_car_shop_1_1_repository_1_1_tests_1_1_repository_tests.html#a251b60a5eb4790a89926482659cde26e',1,'CarShop::Repository::Tests::RepositoryTests']]],
  ['repository_5fread_5fnull',['Repository_Read_Null',['../class_car_shop_1_1_repository_1_1_tests_1_1_repository_tests.html#af994365f6cc395032d51307b83b337ec',1,'CarShop::Repository::Tests::RepositoryTests']]],
  ['repository_5fupdate_5ffake',['Repository_Update_Fake',['../class_car_shop_1_1_repository_1_1_tests_1_1_repository_tests.html#a3402d0a87e4d7a6438a6553efb879a32',1,'CarShop::Repository::Tests::RepositoryTests']]],
  ['repository_5fupdate_5fnull',['Repository_Update_Null',['../class_car_shop_1_1_repository_1_1_tests_1_1_repository_tests.html#a10ec0365a388fd9be44761efdaf19c71',1,'CarShop::Repository::Tests::RepositoryTests']]]
];
