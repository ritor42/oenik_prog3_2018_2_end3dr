var searchData=
[
  ['logic',['Logic',['../class_car_shop_1_1_logic_1_1_logic.html#af006d6f092e7d6adabc05cb8e546d829',1,'CarShop.Logic.Logic.Logic()'],['../class_car_shop_1_1_logic_1_1_logic.html#a7178c39357154d50a27ff138747dd5e8',1,'CarShop.Logic.Logic.Logic(IRepository repository)'],['../class_car_shop_1_1_logic_1_1_tests_1_1_logic_tests.html#a50a5007c30feb39f73916155dd07eb7b',1,'CarShop.Logic.Tests.LogicTests.Logic()']]],
  ['logic_5fcreate_5ffake',['Logic_Create_Fake',['../class_car_shop_1_1_logic_1_1_tests_1_1_logic_tests.html#a5b5dec213e0258bdedb046ff08d26d6c',1,'CarShop::Logic::Tests::LogicTests']]],
  ['logic_5fcreate_5fnull',['Logic_Create_Null',['../class_car_shop_1_1_logic_1_1_tests_1_1_logic_tests.html#a6927af25594942cd4c945df265a7333a',1,'CarShop::Logic::Tests::LogicTests']]],
  ['logic_5fdelete_5ffake',['Logic_Delete_Fake',['../class_car_shop_1_1_logic_1_1_tests_1_1_logic_tests.html#a5ec6d33715add03031489e480fea664c',1,'CarShop::Logic::Tests::LogicTests']]],
  ['logic_5fdelete_5fnull',['Logic_Delete_Null',['../class_car_shop_1_1_logic_1_1_tests_1_1_logic_tests.html#a02bfe770cf12cc10da47210c389fba9f',1,'CarShop::Logic::Tests::LogicTests']]],
  ['logic_5fgroupby_5ffake',['Logic_GroupBy_Fake',['../class_car_shop_1_1_logic_1_1_tests_1_1_logic_tests.html#a1acf7748e978da7dcf6ae0fcc15415de',1,'CarShop::Logic::Tests::LogicTests']]],
  ['logic_5fgroupby_5fnull',['Logic_GroupBy_Null',['../class_car_shop_1_1_logic_1_1_tests_1_1_logic_tests.html#aed5efc9cb6c0922ad90e03328179aadb',1,'CarShop::Logic::Tests::LogicTests']]],
  ['logic_5fgroupbyjoin_5ffake',['Logic_GroupByJoin_Fake',['../class_car_shop_1_1_logic_1_1_tests_1_1_logic_tests.html#afbe57c2f8ebc3a462edd7ebf565002d0',1,'CarShop::Logic::Tests::LogicTests']]],
  ['logic_5fgroupbyjoin_5fnull',['Logic_GroupByJoin_Null',['../class_car_shop_1_1_logic_1_1_tests_1_1_logic_tests.html#a9a1fe517ad27f71dbf4b18c884138aa9',1,'CarShop::Logic::Tests::LogicTests']]],
  ['logic_5fjoin_5ffake',['Logic_Join_Fake',['../class_car_shop_1_1_logic_1_1_tests_1_1_logic_tests.html#ac08c0374f7c04f386cbbbbbec0ecca3f',1,'CarShop::Logic::Tests::LogicTests']]],
  ['logic_5fjoin_5fnull',['Logic_Join_Null',['../class_car_shop_1_1_logic_1_1_tests_1_1_logic_tests.html#ac1bb3003b848d43c87199c92d7df5111',1,'CarShop::Logic::Tests::LogicTests']]],
  ['logic_5fread_5ffake',['Logic_Read_Fake',['../class_car_shop_1_1_logic_1_1_tests_1_1_logic_tests.html#ab56f2d37da1bdd7ca010a65d19dfd12d',1,'CarShop::Logic::Tests::LogicTests']]],
  ['logic_5fread_5fnull',['Logic_Read_Null',['../class_car_shop_1_1_logic_1_1_tests_1_1_logic_tests.html#ae04ba962bb9d166efa91596e3a2ac373',1,'CarShop::Logic::Tests::LogicTests']]],
  ['logic_5fupdate_5ffake',['Logic_Update_Fake',['../class_car_shop_1_1_logic_1_1_tests_1_1_logic_tests.html#ab58b9435f4eed44cc9ac6c7bb892657c',1,'CarShop::Logic::Tests::LogicTests']]],
  ['logic_5fupdate_5fnull',['Logic_Update_Null',['../class_car_shop_1_1_logic_1_1_tests_1_1_logic_tests.html#a993e6a9740dc1d6c863098384391f01f',1,'CarShop::Logic::Tests::LogicTests']]],
  ['logic_5fwebinteraction_5ffake',['Logic_WebInteraction_Fake',['../class_car_shop_1_1_logic_1_1_tests_1_1_logic_tests.html#a2ef7c4c67ef88b8c02b2a77ed3c2601a',1,'CarShop::Logic::Tests::LogicTests']]],
  ['logic_5fwebinteraction_5fnull',['Logic_WebInteraction_Null',['../class_car_shop_1_1_logic_1_1_tests_1_1_logic_tests.html#aa8fcfc33075af1675194c57122b84a9a',1,'CarShop::Logic::Tests::LogicTests']]],
  ['logic_5fwhere_5ffake',['Logic_Where_Fake',['../class_car_shop_1_1_logic_1_1_tests_1_1_logic_tests.html#a0566e20b68769b7cabed9e6f47c0af19',1,'CarShop::Logic::Tests::LogicTests']]],
  ['logic_5fwhere_5fnull',['Logic_Where_Null',['../class_car_shop_1_1_logic_1_1_tests_1_1_logic_tests.html#a60ecf207c961e4f213051c2f0dbfa61c',1,'CarShop::Logic::Tests::LogicTests']]]
];
