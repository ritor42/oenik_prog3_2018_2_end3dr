var class_car_shop_1_1_data_1_1_brand =
[
    [ "Brand", "class_car_shop_1_1_data_1_1_brand.html#aa018f36742fd7057c88e165314ef23af", null ],
    [ "BrandID", "class_car_shop_1_1_data_1_1_brand.html#aec1bc44d2dc3da0757009ffad4809c0c", null ],
    [ "CEO", "class_car_shop_1_1_data_1_1_brand.html#a4c410dac60fbbc354519e0b0a8f12046", null ],
    [ "Headquarter", "class_car_shop_1_1_data_1_1_brand.html#ab56ccb473684f5c55b3418be0d0bea18", null ],
    [ "Model", "class_car_shop_1_1_data_1_1_brand.html#a210c246f96358188488da27646dc7524", null ],
    [ "Name", "class_car_shop_1_1_data_1_1_brand.html#a20d04325324c45cf1530382f552ae78e", null ],
    [ "URL", "class_car_shop_1_1_data_1_1_brand.html#a1c3fe4e12c0802ad185f402aa8d093aa", null ],
    [ "YearlyIncome", "class_car_shop_1_1_data_1_1_brand.html#a6198a6f6df8bbbc88464b29c1e49e159", null ],
    [ "YearOfFoundation", "class_car_shop_1_1_data_1_1_brand.html#a3d128527d502664a5e9df78b72fb6c5b", null ]
];