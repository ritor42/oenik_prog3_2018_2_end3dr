var class_car_shop_1_1_logic_1_1_tests_1_1_logic_tests =
[
    [ "Logic", "class_car_shop_1_1_logic_1_1_tests_1_1_logic_tests.html#a50a5007c30feb39f73916155dd07eb7b", null ],
    [ "Logic_Create_Fake", "class_car_shop_1_1_logic_1_1_tests_1_1_logic_tests.html#a5b5dec213e0258bdedb046ff08d26d6c", null ],
    [ "Logic_Create_Null", "class_car_shop_1_1_logic_1_1_tests_1_1_logic_tests.html#a6927af25594942cd4c945df265a7333a", null ],
    [ "Logic_Delete_Fake", "class_car_shop_1_1_logic_1_1_tests_1_1_logic_tests.html#a5ec6d33715add03031489e480fea664c", null ],
    [ "Logic_Delete_Null", "class_car_shop_1_1_logic_1_1_tests_1_1_logic_tests.html#a02bfe770cf12cc10da47210c389fba9f", null ],
    [ "Logic_GroupBy_Fake", "class_car_shop_1_1_logic_1_1_tests_1_1_logic_tests.html#a1acf7748e978da7dcf6ae0fcc15415de", null ],
    [ "Logic_GroupBy_Null", "class_car_shop_1_1_logic_1_1_tests_1_1_logic_tests.html#aed5efc9cb6c0922ad90e03328179aadb", null ],
    [ "Logic_GroupByJoin_Fake", "class_car_shop_1_1_logic_1_1_tests_1_1_logic_tests.html#afbe57c2f8ebc3a462edd7ebf565002d0", null ],
    [ "Logic_GroupByJoin_Null", "class_car_shop_1_1_logic_1_1_tests_1_1_logic_tests.html#a9a1fe517ad27f71dbf4b18c884138aa9", null ],
    [ "Logic_Join_Fake", "class_car_shop_1_1_logic_1_1_tests_1_1_logic_tests.html#ac08c0374f7c04f386cbbbbbec0ecca3f", null ],
    [ "Logic_Join_Null", "class_car_shop_1_1_logic_1_1_tests_1_1_logic_tests.html#ac1bb3003b848d43c87199c92d7df5111", null ],
    [ "Logic_Read_Fake", "class_car_shop_1_1_logic_1_1_tests_1_1_logic_tests.html#ab56f2d37da1bdd7ca010a65d19dfd12d", null ],
    [ "Logic_Read_Null", "class_car_shop_1_1_logic_1_1_tests_1_1_logic_tests.html#ae04ba962bb9d166efa91596e3a2ac373", null ],
    [ "Logic_Update_Fake", "class_car_shop_1_1_logic_1_1_tests_1_1_logic_tests.html#ab58b9435f4eed44cc9ac6c7bb892657c", null ],
    [ "Logic_Update_Null", "class_car_shop_1_1_logic_1_1_tests_1_1_logic_tests.html#a993e6a9740dc1d6c863098384391f01f", null ],
    [ "Logic_WebInteraction_Fake", "class_car_shop_1_1_logic_1_1_tests_1_1_logic_tests.html#a2ef7c4c67ef88b8c02b2a77ed3c2601a", null ],
    [ "Logic_WebInteraction_Null", "class_car_shop_1_1_logic_1_1_tests_1_1_logic_tests.html#aa8fcfc33075af1675194c57122b84a9a", null ],
    [ "Logic_Where_Fake", "class_car_shop_1_1_logic_1_1_tests_1_1_logic_tests.html#a0566e20b68769b7cabed9e6f47c0af19", null ],
    [ "Logic_Where_Null", "class_car_shop_1_1_logic_1_1_tests_1_1_logic_tests.html#a60ecf207c961e4f213051c2f0dbfa61c", null ],
    [ "Setup", "class_car_shop_1_1_logic_1_1_tests_1_1_logic_tests.html#aaf8c1bba61293291955e033491fbf965", null ]
];