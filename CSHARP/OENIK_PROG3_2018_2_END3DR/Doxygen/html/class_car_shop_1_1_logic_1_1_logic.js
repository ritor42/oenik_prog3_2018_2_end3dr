var class_car_shop_1_1_logic_1_1_logic =
[
    [ "Logic", "class_car_shop_1_1_logic_1_1_logic.html#af006d6f092e7d6adabc05cb8e546d829", null ],
    [ "Logic", "class_car_shop_1_1_logic_1_1_logic.html#a7178c39357154d50a27ff138747dd5e8", null ],
    [ "Create", "class_car_shop_1_1_logic_1_1_logic.html#ae5333a7847bb52d0dd94c8693a82af82", null ],
    [ "Delete", "class_car_shop_1_1_logic_1_1_logic.html#ae971c3ab4fed7f265020a4d07bc95106", null ],
    [ "GroupBy", "class_car_shop_1_1_logic_1_1_logic.html#a6235703e70749d957cee7f414b249a5c", null ],
    [ "GroupByJoin", "class_car_shop_1_1_logic_1_1_logic.html#ada8dae3f09dc79b7d2619f6c2c925991", null ],
    [ "Join", "class_car_shop_1_1_logic_1_1_logic.html#a9efe5d1860f7ea9f8a71477703adc310", null ],
    [ "Read", "class_car_shop_1_1_logic_1_1_logic.html#ab9f0e025ffd240708793adba2843f7ab", null ],
    [ "Update", "class_car_shop_1_1_logic_1_1_logic.html#a6754e54c75d8b58e5ccd3f6115866dd8", null ],
    [ "WebInteraction", "class_car_shop_1_1_logic_1_1_logic.html#a34f1afbfbb9f5ee7070b8288346d0ebf", null ],
    [ "Where", "class_car_shop_1_1_logic_1_1_logic.html#ab8d7a8b843d33b29b6137441663870a3", null ]
];