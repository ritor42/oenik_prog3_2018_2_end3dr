
import java.util.Random;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Bence
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Offer {
    @XmlElement
    String Name;
    
    @XmlElement
    String ModelName;
    
    @XmlElement
    int Price;
    
    @XmlTransient
    private Random rand = new Random();
    
    public Offer() {
    }
    
    public Offer(String name, String modelName, String price) {
        this.Name = name;
        this.ModelName = modelName;
        this.Price = RandomGenerator(Integer.parseInt(price));
    }
    
    private int RandomGenerator(int price) {
        int randomRange = (int) (price*0.2);
        return (int) (rand.nextInt(randomRange) + price * 0.8);
    }
    
    public String getName() {
        return this.Name;
    }
    
    public String getModelName() {
        return this.ModelName;
    }
    
    public int getPrice() {
        return this.Price;
    }
    
    public void setName(String name) {
        this.Name = name;
    }
    
    public void setModelName(String modelName) {
        this.ModelName = modelName;
    }
    
    public void setPrice(int price) {
        this.Price = price;
    }
}
