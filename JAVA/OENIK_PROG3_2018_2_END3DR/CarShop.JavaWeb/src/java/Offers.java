
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Bence
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Offers {
    @XmlElement
    List<Offer> offer;
    
    public Offers() {
    }
    
    public Offers(String name, String model, String price) {
        this.offer = new ArrayList();
        
        this.offer.add(new Offer(name,model,price));
        this.offer.add(new Offer(name,model,price));
        this.offer.add(new Offer(name,model,price));
        this.offer.add(new Offer(name,model,price));
        this.offer.add(new Offer(name,model,price));
    }
}
